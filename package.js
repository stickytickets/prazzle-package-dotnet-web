var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var fs = require('fs');
var rimraf = require('rimraf');
var configure = require('./configure.js');
var log = require('winston');

function createResult(options) {
    return [{
        file: path.resolve(options.packageFilepath),
        metadata: {
            name: options.project,
            version: options.version,
            extension: path.extname(options.packageFilepath),
            packageType: 'dotnet-web',
            deployment: 'server',
            install: options.install,
            env: options.env
        }
    }];
}
module.exports = function(options, cb) {
    log.info('Packing DotNet Web App To: %s', options.outputDir);

    configure.get(options, function() {

        //return if already exists.
        if (fs.existsSync(path.resolve(options.packageFilepath)))
            return cb(null, createResult(options));

        async.waterfall([
            async.apply(publishWebsite, options),
            zipDirectory
        ], function(err) {

            if (err) return cb(err);

            cb(null, createResult(options));
        });
    });
}

function cleanDir(options, cb) {
    options.fn.cleanDir(options.outputDir, function(err) {
        cb(err, options);
    })
}

function publishWebsite(options, cb) {

    /*
    //do nothing code
    fs.mkdirSync(path.join(options.tmpDir, 'test'));
    return cb(null, options);
    */

    log.info('Running Publish command', options.outputDir);


    var cmdTarget = 'cmd';
    var cmdArgs = `/c MSBuild.exe ${options.project}.csproj ` +
        `/t:PipelinePreDeployCopyAllFilesToOneFolder /p:AutoParameterizationWebConfigConnectionStringsDependsOn= /p:Configuration=Release /p:NoAssemblyVersion=Y  /p:_PackageTempDir=${options.tmpDir} /p:BuildStatic=Y`
        // t:Build /p:Configuration=Release /p:OutDir=${options.tmpDir}`; //
    log.verbose('Running:', cmdTarget, cmdArgs);

    var cmd = spawn(cmdTarget, cmdArgs.split(' '));
    var hasError;
    var dataOutput = '';
    cmd.stdout.on('data', (data) => {
        dataOutput += data;
        log.debug(data.toString());
    });

    cmd.stderr.on('data', (data) => {
        hasError = true;
        log.error(data.toString());
    });

    cmd.on('close', (code) => {

        if (hasError || code > 0) {
            log.info(dataOutput);
            return cb('An error occured during the web publish. Exit Code: ' + code);
        }

        log.info('Package project exit code:  ' + code);

        return cb(null, options);
    });

}


function zipDirectory(options, cb) {
    var outputFilename = options.packageFilepath;

    if (fs.existsSync(outputFilename)) fs.unlinkSync(outputFilename);

    //options.fn.zipHelper.zipDir(path.join(options.tmpDir, '_PublishedWebsites', options.project), outputFilename, function(e, r){ cb(e, options) });
    options.fn.zipHelper.zipDir(options.tmpDir, outputFilename, function(e, r) {
        cb(e, options)
    });
}
